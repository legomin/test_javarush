<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf8">
	<title><spring:message code="label.title" /></title>
</head>
<body>

<a href="<c:url value="/logout" />">
	<spring:message code="label.logout" />
</a>
  
<h2><spring:message code="label.title" /></h2>

<form:form method="post" action="add" commandName="contact">

	<table>
		<tr>
			<td><form:label path="id" >
				<spring:message code="label.id" />
			</form:label></td>
			<td><form:input readonly="true" path="id" /></td>
		</tr>
		<tr>
			<td><form:label path="name">
				<spring:message code="label.name" />
			</form:label></td>
			<td><form:input path="name" /></td>
		</tr>
		<tr>
			<td><form:label path="age">
				<spring:message code="label.age" />
			</form:label></td>
			<td><form:input path="age" /></td>
		</tr>
		<tr>
			<td><form:label path="isAdmin">
				<spring:message code="label.isAdmin" />
			</form:label></td>
			<td><form:input path="isAdmin" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit"
				value="<spring:message code="label.addcontact"/>" /></td>
		</tr>

	</table>
</form:form>

<form:form method="post" action="/find" commandName="contact">

	<table>
		<tr>
			<td><form:label path="name">
				<spring:message code="label.name" />
			</form:label></td>
			<td><form:input path="name" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit"
								   value="<spring:message code="label.findcontact"/>" /></td>
		</tr>
	</table>
</form:form>

<h3><spring:message code="label.contacts" /></h3>
<c:if test="${!empty contactList}">
	<table class="data">
		<tr>
			<th><spring:message code="label.id" /></th>
			<th><spring:message code="label.name" /></th>
			<th><spring:message code="label.age" /></th>
			<th><spring:message code="label.isAdmin" /></th>
			<th><spring:message code="label.createdDate" /></th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
		<c:forEach items="${contactList}" var="contact">
			<tr>
				<td>${contact.id}</td>
				<td>${contact.name}</td>
				<td>${contact.age}</td>
				<td>${contact.isAdmin}</td>
				<td>${contact.createdDate}</td>
				<td><a href="/delete/${contact.id}"><spring:message code="label.delete" /></a></td>
				<td><a href="/edit/${contact.id}"><spring:message code="label.edit" /></a></td>
			</tr>
		</c:forEach>
	</table>
	<%--For displaying Page numbers.
   The when condition does not display a link for the current page--%>
	<table >
		<tr>
			<c:forEach begin="1" end="${countPages}" var="i">
				<td><a href="?page=${i}">${i}</a></td>
			</c:forEach>
		</tr>
	</table>
</c:if>

</body>
</html>