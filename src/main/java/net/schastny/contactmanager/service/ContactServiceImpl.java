package net.schastny.contactmanager.service;
 
import java.util.List;

import org.hibernate.*;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.TypedValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.Expression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
 
import net.schastny.contactmanager.dao.ContactDAO;
import net.schastny.contactmanager.domain.Contact;

import javax.annotation.Resource;

@Service
public class ContactServiceImpl implements ContactService {
 
    @Autowired
    private ContactDAO contactDAO;

    @Resource(name="sessionFactory")
    private SessionFactory sessionFactory;

    @Transactional
    public void addContact(Contact contact) {
        contactDAO.addContact(contact);
    }
 
    @Transactional
    public List<Contact> listContact() {
 
        return contactDAO.listContact();
    }

    @Transactional
    public List<Contact> listContact(int beginRow,int maxRows) {

        return contactDAO.listContact(beginRow,maxRows);
    }


    @Transactional
    public void removeContact(Integer id) {
        contactDAO.removeContact(id);
    }

    @Transactional
    public Contact getContact(Integer id) {
        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person first
        Contact person = (Contact) session.get(Contact.class, id);

        return person;
    }

    @Transactional
    public void edit(Contact person) {

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person via id
        Contact existingPerson = (Contact) session.get(Contact.class, person.getId());

        // Assign updated values to this person
        existingPerson.setName(person.getName());
        existingPerson.setAge(person.getAge());
        existingPerson.setIsAdmin(person.getIsAdmin());

        // Save updates
        session.save(existingPerson);
    }

    @Transactional
    public List<Contact> findContacts(Contact contact)
    {
        Session session = sessionFactory.getCurrentSession();

        StringBuilder query = new StringBuilder("from Contact ");

        if ( (!contact.getName().equals(""))&&contact.getName()!=null )
            query.append(" where name = '"+contact.getName()+"'");



        //session.
        return session.createQuery(query.toString()).list();
    }

    @Transactional
    public int getCountPages(int countSPerPage){
        int countStrings = listContact().size();
        if (countStrings%countSPerPage==0)
            return countStrings/countSPerPage;
        else return countStrings/countSPerPage+1;

    }
}
