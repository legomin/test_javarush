package net.schastny.contactmanager.service;

import java.util.List;
import net.schastny.contactmanager.domain.Contact;

public interface ContactService {

	public void addContact(Contact contact);

	public List<Contact> listContact();

	public List<Contact> listContact(int beginRow,int maxRows);

	public void removeContact(Integer id);

	public Contact getContact(Integer id);

	public void edit(Contact person);

	public List<Contact> findContacts(Contact contact);

	public int getCountPages(int countSPerPage);
}
