package net.schastny.contactmanager.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "USER")
public class Contact {

	@Id
	@Column(name = "ID")
	@GeneratedValue
	private Integer id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "AGE")
	private int age;

	@Column(name = "ISADMIN")
	private Boolean isAdmin;

	@Column(name = "CREATEDDATE")
	private Date createdDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setIsAdmin(Boolean admin) {
		isAdmin = admin;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}
