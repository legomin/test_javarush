package net.schastny.contactmanager.web;

import java.util.Map;

import net.schastny.contactmanager.domain.Contact;
import net.schastny.contactmanager.service.ContactService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
public class ContactController {

	@Autowired
	private ContactService contactService;

	private int countStringsPerPage = 5;

	@RequestMapping("/index")
	public String listContacts(Map<String, Object> map, Integer page) {
		int p=0;
		if (page==null)
			p=1;
		else p=page;
		int countPages = contactService.getCountPages(countStringsPerPage);

		map.put("contact", new Contact());
		map.put("contactList", contactService.listContact((p-1)*countStringsPerPage,countStringsPerPage));


		map.put("countPages",countPages);

		return "contact";
	}
	
	@RequestMapping("/")
	public String home() {
		return "redirect:/index?page=1";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addContact(@ModelAttribute("contact") Contact contact,
			BindingResult result, Integer page) {

		contactService.addContact(contact);

		return "redirect:/index?page="+(page==null ? 1 : page);
	}

	@RequestMapping("/delete/{contactId}")
	public String deleteContact(@PathVariable("contactId") Integer contactId, Integer page) {

		contactService.removeContact(contactId);

		return "redirect:/index?page="+(page==null ? 1 : page);
	}

	@RequestMapping(value = "/edit/{contactId}")
	public String getEdit(@PathVariable("contactId") Integer contactId, Map<String, Object> map, Integer page) {

		int p=0;
		if (page==null)
			p=1;
		else p=page;
		int countPages = contactService.getCountPages(countStringsPerPage);

		map.put("contact",contactService.getContact(contactId));
		map.put("contactList", contactService.listContact((p-1)*countStringsPerPage,countStringsPerPage));


		map.put("countPages",countPages);

		return "contact";

	}

	@RequestMapping(value = "/edit/add",method = RequestMethod.POST)
	public String Edit(@ModelAttribute("contact") Contact contact,
					   BindingResult result, Integer page) {
		contactService.edit(contact);

		return "redirect:/index?page="+(page==null ? 1 : page);

	}

	@RequestMapping(value = "/find")
	public String FindContacts(@ModelAttribute("contact") Contact contact,
					   BindingResult result,Map<String, Object> map, Integer page) {
		map.put("contact", new Contact());
		map.put("contactList", contactService.findContacts(contact));

		return "contact";

	}
}
